package view;

import java.util.ArrayList;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IntegersBag;

public class MVCExample {
	
	
	
	
	private static void printMenu(){
		System.out.println("1. Create a new bag of integers (i.e., a set of unique integers)");
		System.out.println("2. Compute the mean");
		System.out.println("3. Get the max value");
		System.out.println("4. Get the summatorie of all values");
		System.out.println("5. Get the number of pars");
		System.out.println("6. Know if some number exist in the bag");
		System.out.println("7. Exit");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
	
	private static ArrayList<Integer> readData(){
		ArrayList<Integer> data = new ArrayList<Integer>();
		
		System.out.println("Please type the numbers (separated by spaces), then press enter: ");
		try{
			
			String line = new Scanner(System.in).nextLine();
		
			String[] values = line.split("\\s+");
			for(String value: values){
				data.add(new Integer(value.trim()));
			}
		}catch(Exception ex){
			System.out.println("Please type integer numbers separated by spaces");
		}
		return data;
	}

	public static void main(String[] args){
		
		IntegersBag bag = null;
		Scanner sc = new Scanner(System.in);
		
		for(;;){
		  printMenu();
		  
		 
		  int option = sc.nextInt();
		  switch(option){
			  case 1: bag = Controller.createBag(readData()); System.out.println("--------- \n The bag was created  \n---------");
			  break;
			  
			  case 2: System.out.println("--------- \n The mean value is "+Controller.getMean(bag)+" \n---------");
			  break;
			  
			  case 3: System.out.println("--------- \nThe max value is "+Controller.getMax(bag)+" \n---------");		  
			  break;
			  
			  case 4: System.out.print("--------- \nThe summatorie is "+ Controller.sumOfAllValues(bag)+ "\n---------");
			  break;

				case 5: System.out.println("--------- \nThe number of pars is "+Controller.numberOfPars(bag)+" \n---------");		 

				

				case 6:{ 
					sc.nextLine();
					System.out.print("Introduce the number to search: ");
					int n = sc.nextInt();
					System.out.print("--------- \nThe number " + n + " ");
					boolean isHere = Controller.isValueInBag(bag, n);
					if(isHere)
						System.out.print("exists");
					else{
						System.out.println("doesn't exist");
					}

					System.out.println("\n---------");
					break;
				}


				case 7: System.out.println("Bye!!  \n---------"); sc.close(); return;		  


				default: System.out.println("--------- \n Invalid option !! \n---------");
				}
		}
	}
	
}
