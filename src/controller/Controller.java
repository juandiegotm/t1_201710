package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
	public static int numberOfPars(IntegersBag bag)
	{
		return model.numberOfPairs(bag);
	}
	
	public static boolean isValueInBag(IntegersBag bag, int n)
	{
		return model.isValueInBag(bag, n);
	}
	
	public static double sumOfAllValues(IntegersBag bag){
		return model.sumOfAllValues(bag);
	}
	
}
