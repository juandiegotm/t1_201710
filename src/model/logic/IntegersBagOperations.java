package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int sumOfAllValues (IntegersBag bag){
		int summatorie = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				summatorie += iter.next().doubleValue();
			}
		}
		
		return summatorie;
	}
	

	public int numberOfPairs(IntegersBag bag)
	{
		int pars = 0;
		
		if(bag != null){
			Iterator<Integer> it = bag.getIterator();
			while(it.hasNext()){
				double actual = it.next().doubleValue();
				if(actual % 2 == 0)
					pars++;
			}
		}
		
		return pars;
	}
	
	
	
	public boolean isValueInBag(IntegersBag bag, int n)
	{
		boolean exist = false;
		if(bag != null){
			
			Iterator<Integer> it = bag.getIterator();
			while(!exist && it.hasNext())
			{
				int actual = it.next().intValue();
				if(actual == n) {
					exist = true;
				}
			}
		}
		
		return exist;
	}
	
	

}
